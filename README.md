# README #

This is the repository for 10bob, a small C# program (built in Visual Studio 2015) to convert pre-decimal GBP currency to decimal.

### What is this repository for? ###

* Just this app, really.

### How do I get set up? ###

* Summary of set up
You can download the repository from here if you want to do things to it in Visual Studio. I'll also upload an MSI to my site ([www.azuregem.com](Link URL))

* Configuration
You shouldn't need to configure much. The Visual Studio Installer can be a bit temperamental with this project, for some reason, but it works.

* Dependencies
You'll need the .NET Framework version 4.5.

### Contribution guidelines ###

* Writing tests
You can if you want.

* Code review
If you like.

### Who do I talk to? ###

* Repo owner or admin
Charlotte Lunn - please contact me through here or through azuregem.com