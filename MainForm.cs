﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _10bob
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void impDecButton_Click(object sender, EventArgs e)
        {
            string errorMsg = "Sorry, one or more boxes contain an invalid entry.";     // setting so it doesn't get repeated

            if (poundBox.Text == "" && shillingBox.Text == "" && penceBox.Text == "")
            {
                MessageBox.Show(null, "The required boxes are still empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else
            {
                long pounds;
                int shillings;
                float pence;
                bool oneError = false;              // this gets set to true if an error is encountered in parsing

                /* This section checks if any of the boxes are blank (assuming they're not all blank),
                 * and sets them to zero if they are. */

                if (poundBox.Text == "" || poundBox.Text == " ")
                {
                    poundBox.Text = "0";
                }

                if (shillingBox.Text == "" || shillingBox.Text == " ")
                {
                    shillingBox.Text = "0";
                }

                if (penceBox.Text == "" || penceBox.Text == " ")
                {
                    penceBox.Text = "0";
                }

                /* This section will try to parse the contents of the relevant text boxes.
                 * If it fails at any point, the program will display an error and will not go any further. */

                if (!long.TryParse(poundBox.Text, out pounds))
                {
                    oneError = true;
                    MessageBox.Show(null, errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (!Int32.TryParse(shillingBox.Text, out shillings) && !oneError)
                {
                    oneError = true;
                    MessageBox.Show(null, errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (!float.TryParse(penceBox.Text, out pence) && !oneError)
                {
                    oneError = true;
                    MessageBox.Show(null, errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (!oneError) {
                    PoundSD impDec = new PoundSD(pounds, shillings, pence);
                    decimalBox.Text = impDec.Decimal.ToString("0.000");
                }
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            poundBox.Text = "";
            shillingBox.Text = "";
            penceBox.Text = "";
            decimalBox.Text = "";
        }

        private void decImpButton_Click(object sender, EventArgs e)
        {
            double newpounds;

            if (decimalBox.Text == "")
            {
                MessageBox.Show(null, "Decimal box is empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else
            {
                if(Double.TryParse(decimalBox.Text, out newpounds))
                {
                    PoundSD decImp = new PoundSD(newpounds);
                    poundBox.Text = decImp.Pounds.ToString();
                    shillingBox.Text = decImp.Shillings.ToString();
                    penceBox.Text = decImp.Pence.ToString("0");
                } else
                {
                    MessageBox.Show(null, "Sorry, one or more boxes contain an invalid entry.",
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
