﻿namespace _10bob
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tenBobHeader = new System.Windows.Forms.Label();
            this.lsdLabel = new System.Windows.Forms.Label();
            this.poundBox = new System.Windows.Forms.TextBox();
            this.shillingBox = new System.Windows.Forms.TextBox();
            this.penceBox = new System.Windows.Forms.TextBox();
            this.decimalLabel = new System.Windows.Forms.Label();
            this.decimalBox = new System.Windows.Forms.TextBox();
            this.solidusLabel = new System.Windows.Forms.Label();
            this.dLabel = new System.Windows.Forms.Label();
            this.pointLabel = new System.Windows.Forms.Label();
            this.oldPoundLabel = new System.Windows.Forms.Label();
            this.newPoundLabel = new System.Windows.Forms.Label();
            this.impDecButton = new System.Windows.Forms.Button();
            this.decImpButton = new System.Windows.Forms.Button();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tenBobHeader
            // 
            this.tenBobHeader.AutoSize = true;
            this.tenBobHeader.Font = new System.Drawing.Font("MS Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tenBobHeader.ForeColor = System.Drawing.Color.Coral;
            this.tenBobHeader.Location = new System.Drawing.Point(12, 15);
            this.tenBobHeader.Name = "tenBobHeader";
            this.tenBobHeader.Size = new System.Drawing.Size(145, 48);
            this.tenBobHeader.TabIndex = 0;
            this.tenBobHeader.Text = "10bob";
            // 
            // lsdLabel
            // 
            this.lsdLabel.AutoSize = true;
            this.lsdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsdLabel.Location = new System.Drawing.Point(12, 106);
            this.lsdLabel.Name = "lsdLabel";
            this.lsdLabel.Size = new System.Drawing.Size(101, 13);
            this.lsdLabel.TabIndex = 1;
            this.lsdLabel.Text = "Imperial Amount:";
            // 
            // poundBox
            // 
            this.poundBox.Location = new System.Drawing.Point(147, 103);
            this.poundBox.Name = "poundBox";
            this.poundBox.Size = new System.Drawing.Size(55, 20);
            this.poundBox.TabIndex = 2;
            // 
            // shillingBox
            // 
            this.shillingBox.Location = new System.Drawing.Point(219, 103);
            this.shillingBox.Name = "shillingBox";
            this.shillingBox.Size = new System.Drawing.Size(24, 20);
            this.shillingBox.TabIndex = 3;
            // 
            // penceBox
            // 
            this.penceBox.Location = new System.Drawing.Point(271, 103);
            this.penceBox.Name = "penceBox";
            this.penceBox.Size = new System.Drawing.Size(24, 20);
            this.penceBox.TabIndex = 4;
            // 
            // decimalLabel
            // 
            this.decimalLabel.AutoSize = true;
            this.decimalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.decimalLabel.Location = new System.Drawing.Point(12, 149);
            this.decimalLabel.Name = "decimalLabel";
            this.decimalLabel.Size = new System.Drawing.Size(102, 13);
            this.decimalLabel.TabIndex = 5;
            this.decimalLabel.Text = "Decimal Amount:";
            // 
            // decimalBox
            // 
            this.decimalBox.Location = new System.Drawing.Point(147, 146);
            this.decimalBox.Name = "decimalBox";
            this.decimalBox.Size = new System.Drawing.Size(100, 20);
            this.decimalBox.TabIndex = 6;
            // 
            // solidusLabel
            // 
            this.solidusLabel.AutoSize = true;
            this.solidusLabel.Location = new System.Drawing.Point(249, 106);
            this.solidusLabel.Name = "solidusLabel";
            this.solidusLabel.Size = new System.Drawing.Size(12, 13);
            this.solidusLabel.TabIndex = 7;
            this.solidusLabel.Text = "/";
            // 
            // dLabel
            // 
            this.dLabel.AutoSize = true;
            this.dLabel.Location = new System.Drawing.Point(301, 106);
            this.dLabel.Name = "dLabel";
            this.dLabel.Size = new System.Drawing.Size(13, 13);
            this.dLabel.TabIndex = 8;
            this.dLabel.Text = "d";
            // 
            // pointLabel
            // 
            this.pointLabel.AutoSize = true;
            this.pointLabel.Location = new System.Drawing.Point(203, 106);
            this.pointLabel.Name = "pointLabel";
            this.pointLabel.Size = new System.Drawing.Size(10, 13);
            this.pointLabel.TabIndex = 9;
            this.pointLabel.Text = ".";
            // 
            // oldPoundLabel
            // 
            this.oldPoundLabel.AutoSize = true;
            this.oldPoundLabel.Location = new System.Drawing.Point(128, 106);
            this.oldPoundLabel.Name = "oldPoundLabel";
            this.oldPoundLabel.Size = new System.Drawing.Size(13, 13);
            this.oldPoundLabel.TabIndex = 10;
            this.oldPoundLabel.Text = "£";
            // 
            // newPoundLabel
            // 
            this.newPoundLabel.AutoSize = true;
            this.newPoundLabel.Location = new System.Drawing.Point(128, 149);
            this.newPoundLabel.Name = "newPoundLabel";
            this.newPoundLabel.Size = new System.Drawing.Size(13, 13);
            this.newPoundLabel.TabIndex = 11;
            this.newPoundLabel.Text = "£";
            // 
            // impDecButton
            // 
            this.impDecButton.Location = new System.Drawing.Point(10, 183);
            this.impDecButton.Name = "impDecButton";
            this.impDecButton.Size = new System.Drawing.Size(114, 40);
            this.impDecButton.TabIndex = 12;
            this.impDecButton.Text = "Imperial -> Decimal";
            this.impDecButton.UseVisualStyleBackColor = true;
            this.impDecButton.Click += new System.EventHandler(this.impDecButton_Click);
            // 
            // decImpButton
            // 
            this.decImpButton.Location = new System.Drawing.Point(131, 183);
            this.decImpButton.Name = "decImpButton";
            this.decImpButton.Size = new System.Drawing.Size(116, 40);
            this.decImpButton.TabIndex = 13;
            this.decImpButton.Text = "Decimal -> Imperial";
            this.decImpButton.UseVisualStyleBackColor = true;
            this.decImpButton.Click += new System.EventHandler(this.decImpButton_Click);
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(20, 63);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(145, 13);
            this.descriptionLabel.TabIndex = 14;
            this.descriptionLabel.Text = "A simple conversion program.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(144, 226);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "(C)2016 Charlotte Lunn (AzureGem)";
            this.toolTip1.SetToolTip(this.label1, "That\'s me...");
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(253, 183);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(61, 40);
            this.clearButton.TabIndex = 16;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // MainForm
            // 
            this.AcceptButton = this.impDecButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 245);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.decImpButton);
            this.Controls.Add(this.impDecButton);
            this.Controls.Add(this.newPoundLabel);
            this.Controls.Add(this.oldPoundLabel);
            this.Controls.Add(this.pointLabel);
            this.Controls.Add(this.dLabel);
            this.Controls.Add(this.solidusLabel);
            this.Controls.Add(this.decimalBox);
            this.Controls.Add(this.decimalLabel);
            this.Controls.Add(this.penceBox);
            this.Controls.Add(this.shillingBox);
            this.Controls.Add(this.poundBox);
            this.Controls.Add(this.lsdLabel);
            this.Controls.Add(this.tenBobHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(344, 284);
            this.MinimumSize = new System.Drawing.Size(344, 284);
            this.Name = "MainForm";
            this.Text = "10bob";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tenBobHeader;
        private System.Windows.Forms.Label lsdLabel;
        private System.Windows.Forms.TextBox poundBox;
        private System.Windows.Forms.TextBox shillingBox;
        private System.Windows.Forms.TextBox penceBox;
        private System.Windows.Forms.Label decimalLabel;
        private System.Windows.Forms.TextBox decimalBox;
        private System.Windows.Forms.Label solidusLabel;
        private System.Windows.Forms.Label dLabel;
        private System.Windows.Forms.Label pointLabel;
        private System.Windows.Forms.Label oldPoundLabel;
        private System.Windows.Forms.Label newPoundLabel;
        private System.Windows.Forms.Button impDecButton;
        private System.Windows.Forms.Button decImpButton;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button clearButton;
    }
}

