﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10bob
{
    class PoundSD
    {
        // PoundSD class
        // Contains currency in £sd format.
        //
        // Charlotte Lunn, June 2016

        private long _pounds;
        private int _shillings;
        private float _pence;
        private double _decimal;
        
        private int penceInShilling = 12;
        private int shillingsInPound = 20;

        // Constructors below.

        public PoundSD()
            : this(0, 0, 0)
        {
            // default constructor
        }

        public PoundSD(long pounds)
            : this(pounds, 0, 0)
        {
            // constructor
        }

        public PoundSD(long pounds, int shillings)
            : this(pounds, shillings, 0)
        {
            // constructor
        }

        public PoundSD(long pounds, int shillings, float pence)
        {
            this.Pounds = pounds;
            this.Shillings = shillings;
            this.Pence = pence;
        }

        public PoundSD(double newpounds)
        {
            this.Decimal = newpounds;
        }

        // end of constructors

        /* This section is for the "pence and shillings to fraction" methods.
         * They're pretty simple. */

        private float penceToFrac()
        {
            float fraction;
            fraction = this.Pence / this.penceInShilling / this.shillingsInPound;
            return fraction;
        }

        private float shillToFrac()
        {
            float fraction;
            fraction = (float)this.Shillings / this.shillingsInPound;
            return fraction;
        }

        private void updateDecimal()
        {
            float penceFrac = penceToFrac();
            float shillFrac = shillToFrac();
            _decimal = this.Pounds + shillFrac + penceFrac;
        }

        /* Old-fashioned getters and setters below. Bit of extra stuff going on than if they were just plain
         * properties, though. */

        public long Pounds
        {
            get
            {
                return _pounds;
            }

            set
            {
                _pounds = value;
                updateDecimal();
            }
        }

        public int Shillings
        {
            get
            {
                return _shillings;
            }

            set
            {
                _shillings = value;
                updateDecimal();
            }
        }

        public float Pence
        {
            get
            {
                return _pence;
            }

            set
            {
                _pence = value;
                updateDecimal();
            }
        }

        private void DecimalToImp ()
        {
            _pounds = (long)this.Decimal;
            double shillingDbl = (this.Decimal - this.Pounds) * this.shillingsInPound;
            _shillings = (int)shillingDbl;
            double penceDbl = (this.Decimal - this.Pounds - shillToFrac()) * 100;
            penceDbl = penceDbl / 5 * 12;
            _pence = (float)penceDbl;
        }

        public double Decimal
        {

            get
            {
                return _decimal;
            }

            set
            {
                _decimal = value;
                DecimalToImp();
            }
        }

    }
}
